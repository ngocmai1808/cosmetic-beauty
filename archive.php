<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package cosmetic
 */

get_header();
?>
<div class="container-fluid">

<div class="breadcrumbs">
<?php
	the_archive_title( '<h1 class="page-title">', '</h1>' );
	?>
<?php
if ( function_exists('yoast_breadcrumb') ) {
yoast_breadcrumb('
<p id="breadcrumbs">','</p>');
}
?>
</div>

<div class="row">
	<div class="col-md-9 col-md-push-3">
		<div id="primary" class="content-area">
			<main id="main" class="site-main">

			<?php if ( have_posts() ) : ?>

				<header class="page-header">
					
				</header><!-- .page-header -->
				<div class="row">
				<?php
				/* Start the Loop */
				while ( have_posts() ) :
					the_post();?>
					<div class="col-md-6">
						<?php
							/*
							 * Include the Post-Type-specific template for the content.
							 * If you want to override this in a child theme, then include a file
							 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
							 */
							get_template_part( 'template-parts/content', get_post_type() );?>
					</div>
					

				<?php endwhile;

				the_posts_navigation();

			else :

				get_template_part( 'template-parts/content', 'none' );

			endif;
			?>
				</div>
			</main><!-- #main -->
		</div><!-- #primary -->
	</div>
	<div class="col-md-3 col-md-pull-9">
		<?php get_sidebar();?>
	</div>
</div>
</div>
	

<?php 
get_footer();
