<?php
// Khoi tao widget
add_action( 'widgets_init', 'create_widget_news' );
function create_widget_news() {
	register_widget( 'Display_Recent_Post_By_Category' );
}
// Tao widget
class Display_Recent_Post_By_Category extends WP_Widget {	

	//Thong tin widget
	function __construct(){
		parent::__construct(
			'display_recent_post',//id dai dien cho widget
			'Bài viết theo danh mục',//ten widget
			array(
				'description' => 'Bài viết theo danh mục'//mieu ta
			)
		);
	}

	//Thiet lap truong nhap lieu
	function form( $instance ) {
		// echo '<pre>';
		// print_r($instance);
		// echo '</pre>';
		$default = array(
			'title' => '',
			'post_number' 	=> 3,
			'category' 		=> '',
		);
		$instance = wp_parse_args( $instance, $default );	
		
		echo 'Tiêu đề: <input type="text" class="widefat" name='.$this->get_field_name( 'title' ).' value="'.$instance["title"].'" />';
		echo 'Số bài viết: <input type="text" class="widefat" 
		name='.$this->get_field_name( 'post_number' ).' value="'.$instance["post_number"].'" />';
?>
		<p>
			<label for="<?php echo $this->get_field_id("category"); ?>">
				<?php 
					_e("Danh mục: ", "cosmetic") . "<br />"; 
					wp_dropdown_categories(	array(
						'hide_empty'	=> 1,
						'taxonomy' 		=> 'category', 
						'name' 			=> $this->get_field_name("category"), 
						'selected' 		=> $instance["category"]
					)); 
				?>
			</label>
		</p>
<?php
	}

	//Luu du lieu tu form
	function update($new_instance, $old_instance){
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['post_number'] = strip_tags($new_instance['post_number']);
		$instance['category'] = strip_tags($new_instance['category']);
		return $instance;
	}

	//Hien thi widget ra ben ngoai
	function widget( $args, $instance ) {
		extract($args);

		echo $before_widget;
		echo $before_title ?>
		<a href=""><?php echo $instance['title'] ?></a><?php echo $after_title; ?>
			<div class="short-desc">
				<span>Just in now</span>
			</div>
		<?php $args = array(
			'post_type' => 'post',
			'post_status' => 'publish',
			'tax_query' => array(
				array(
					'taxonomy'  => 'category',
					'field'     => 'id',
					'terms'     => $instance ['category']
				)
			),
			'orderby' => 'id',
			'order' => 'DESC',
			'posts_per_page' => $instance['post_number'], 
		);
		$query = new WP_Query( $args );	
		// echo '<pre>';
		// print_r($query->posts);
		// echo '</pre>';

?>			
	    <div class="new-sm">
				<div class="owl-theme owl-carousel owl-magazine">
	    	<?php $i = 0; ?>
			<?php while($query->have_posts()) : $query->the_post(); ?>
				<div class="item">
					<?php get_template_part('template-parts/content','post'); ?>
				</div>
			<?php endwhile; wp_reset_postdata(); ?>
	    </div>
        
<?php echo $after_widget;
	}
}
?>

