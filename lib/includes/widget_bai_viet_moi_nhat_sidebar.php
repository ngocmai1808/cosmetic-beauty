<?php
// Khoi tao widget
add_action( 'widgets_init', 'create_widget_news_sidebar' );
function create_widget_news_sidebar() {
	register_widget( 'Display_Recent_Post_By_Category_Sidebar' );
}
// Tao widget
class Display_Recent_Post_By_Category_Sidebar extends WP_Widget {	

	//Thong tin widget
	function __construct(){
		parent::__construct(
			'display_recent_post_sidebar',//id dai dien cho widget
			'Bài viết mới nhất sidebar',//ten widget
			array(
				'description' => 'Bài viết mới nhất sidebar'//mieu ta
			)
		);
	}

	//Thiet lap truong nhap lieu
	function form( $instance ) {
		echo '<pre>';
		print_r($default);
		echo '</pre>';
		$default = array(
			'title' => '',
			'post_number' 	=> 3,
		);
	
		$instance = wp_parse_args( $instance, $default );	
		
		echo 'Tiêu đề: <input type="text" class="widefat" name='.$this->get_field_name( 'title' ).' value="'.$instance["title"].'" />';
		echo 'Số bài viết: <input type="text" class="widefat" 
		name='.$this->get_field_name( 'post_number' ).' value="'.$instance["post_number"].'" />';
?>
		
<?php
	}

	//Luu du lieu tu form
	function update($new_instance, $old_instance){
		parent::update( $new_instance, $old_instance );

		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['post_number'] = strip_tags($new_instance['post_number']);
		return $instance;
	}

	//Hien thi widget ra ben ngoai
	function widget( $args, $instance ) {
		extract($args);

		echo $before_widget;
		echo $before_title ?>
		<a href=""><?php echo $instance['title'] ?></a><?php echo $after_title; 
 
		$args = array(
			'post_type' => 'post',
			'post_status' => 'publish',
			
			'orderby' => 'id',
			'order' => 'DESC',
			'posts_per_page' => $instance['post_number'], 
		);
		$query = new WP_Query( $args );	
		// echo '<pre>';
		// print_r($query->posts);
		// echo '</pre>';

?>			
	    <div class="new-sm">
			<?php while($query->have_posts()) : $query->the_post(); ?>
				<div class="item">
					<div class="post">
						<div class="row">
							<div class="col-md-4">
								<div class="post-img">
									<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'full' ); ?></a>
								</div>
							</div>
							<div class="col-md-8">
								<div class="post-content">
									<div class="post-title">
										<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
									</div>
									<div class="readmore"><a href="<?php the_permalink(); ?>">Read more</a></div>
								</div>
							</div>
						</div>
						
						
					</div>
				</div>
			<?php endwhile; wp_reset_postdata(); ?>
	 
<?php echo $after_widget;
	}
}
?>

