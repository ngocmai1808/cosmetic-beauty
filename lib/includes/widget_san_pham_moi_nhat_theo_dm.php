<?php
// Khoi tao widget
add_action( 'widgets_init', 'create_widget_product' );
function create_widget_product() {
	register_widget( 'Display_Recent_Product_By_Category' );
}
// Tao widget
class Display_Recent_Product_By_Category extends WP_Widget {	

	//Thong tin widget
	function __construct(){
		parent::__construct(
			'display_recent_product',//id dai dien cho widget
			'Sản phẩm theo danh mục',//ten widget
			array(
				'description' => 'Sản phẩm theo danh mục'//mieu ta
			)
		);
	}

	//Thiet lap truong nhap lieu
	function form( $instance ) {
		// echo '<pre>';
		// print_r($instance);
		// echo '</pre>';
		$default = array(
			'title' => '',
			'product_number' 	=> 3,
			'product_category' 		=> '',
		);
		$instance = wp_parse_args( $instance, $default );	
		
		echo 'Tiêu đề: <input type="text" class="widefat" name='.$this->get_field_name( 'title' ).' value="'.$instance["title"].'" />';
		echo 'Số bài viết: <input type="text" class="widefat" 
		name='.$this->get_field_name( 'product_number' ).' value="'.$instance["product_number"].'" />';
?>
		<p>
			<label for="<?php echo $this->get_field_id("product_category"); ?>">
				<?php 
					_e("Danh mục: ", "cosmetic") . "<br />"; 
					wp_dropdown_categories(	array(
						'hide_empty'	=> 0,
						'taxonomy' 		=> 'product_cat', 
						'show_option_all' => __( 'Tất cả', 'cosmetic' ),
						'name' 			=> $this->get_field_name("product_category"), 
						'selected' 		=> $instance["product_category"]
					)); 
				?>
			</label>
		</p>
<?php
	}

	//Luu du lieu tu form
	function update($new_instance, $old_instance){
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['product_number'] = strip_tags($new_instance['product_number']);
		$instance['product_category'] = strip_tags($new_instance['product_category']);
		return $instance;
	}

	//Hien thi widget ra ben ngoai
	function widget( $args, $instance ) {
		extract($args);
		$term = get_term( $instance['product_category'], 'product_cat' );
		// var_dump($term);
		echo $before_widget;
		echo $before_title;

		echo '<a href="'. get_term_link($term->slug, 'product_cat') . '">'. $instance['title']. '</a>' ;

		echo $after_title; ?>
			<div class="short-desc">
				<span>Just in now</span>
			</div>
		
		<?php $args = array(
			'post_type' => 'product',
			'post_status' => 'publish',
			'tax_query' => array(
				array(
					'taxonomy'  => 'product_cat',
					'field'     => 'id',
					'terms'     => $instance ['product_category']
				)
			),
			'orderby' => 'id',
			'order' => 'DESC',
			'posts_per_page' => $instance['product_number'], 
		);
		$the_query = new WP_Query( $args );?>

		<div class="new-sm">
				<div class="owl-theme owl-carousel owl-home">
				<?php // The Loop
				if ( $the_query->have_posts() ) :
					while ( $the_query->have_posts() ) : $the_query->the_post();
						global $product;
		                if ( ! is_a( $product, 'WC_Product' ) ) {
		                    return;
		                }
		                ?>
						<div class="item">
							<?php get_template_part('template-parts/content','product-home'); ?>
						</div>
				
				<?php endwhile;
				// Reset Post Data
				wp_reset_postdata();
				endif;
				?>		
			</div>
		</div>	
	<?php echo $after_widget;
	}
}
?>

