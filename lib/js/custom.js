jQuery(document).ready(function($) {
	$('.owl-banner').owlCarousel({
        loop:true,
        nav:false,
        dots:false,
        items:1,
        autoplay:true,
        autoplayTimeout:5000,
        autoplayHoverPause:true,
        animateOut: 'slideOutDown',
        animateIn: 'flipInX',
        stagePadding:0,
        smartSpeed:450
    });
    $('.owl-home').owlCarousel({
        loop:true,
        nav:true,
        dots:false,
        items:6,
        margin:15,
        autoplay:true,
        autoplayTimeout:5000,
        autoplayHoverPause:true,
        animateOut: 'slideOutDown',
        animateIn: 'flipInX',
        stagePadding:0,
        smartSpeed:450

    });
    $('.owl-magazine').owlCarousel({
        loop:true,
        nav:false,
        dots:false,
        items:2,
        margin:15,
        autoplay:true,
        autoplayTimeout:5000,
        autoplayHoverPause:true,
        animateOut: 'slideOutDown',
        animateIn: 'flipInX',
        stagePadding:0,
        smartSpeed:450

    });
    $('.owl-social').owlCarousel({
        loop:true,
        nav:false,
        dots:false,
        items:1,
        autoplay:true,
        autoplayTimeout:1000,
        autoplayHoverPause:true,
        animateOut: 'slideOutDown',
        animateIn: 'flipInX',
        stagePadding:0,
        smartSpeed:450
    });
});
jQuery(document).ready(function($) {
    $('.back-to-top').click(function(event) {
        /* Act on the event */
        $('html, body').animate({scrollTop:0},1500);

    });

    $('.list-social-icon').click(function(event) {
        /* Act on the event */
       
        $('.list-social ul').toggleClass('social-hien');
    });
    $('section, .banner').click(function(event) {
        /* Act on the event */
      $('.list-social ul').removeClass('social-hien');
    });
});
