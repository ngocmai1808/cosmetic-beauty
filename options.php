<?php
/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 * By default it uses the theme name, in lowercase and without spaces, but this can be changed if needed.
 * If the identifier changes, it'll appear as if the options have been reset.
 */

function optionsframework_option_name() {
	
	$themename = get_option( 'stylesheet' );
	$themename = preg_replace("/\W/", "_", strtolower($themename) );
	$optionsframework_settings = get_option('optionsframework');
	$optionsframework_settings['id'] = $themename;
	update_option('optionsframework', $optionsframework_settings);
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 *
 * If you are making your theme translatable, you should replace 'cosmetic'
 * with the actual text domain for your theme.  Read more:
 * http://codex.wordpress.org/Function_Reference/load_theme_textdomain
 */

function optionsframework_options() {

	// Pull all the categories into an array
	$options_categories = array();
	$options_categories_obj = get_categories();
	foreach ($options_categories_obj as $category) {
		$options_categories[$category->cat_ID] = $category->cat_name;
	}

	// Pull all the taxonomy into an array
	$options_taxonomy = array();
	$terms = get_terms(array(
		'taxonomy'	=>	'product_cat',
		'parent'	=>	0
		));

	if ( $terms && !is_wp_error( $terms ) ){
		$options_taxonomy[''] ='Vui lòng chọn danh mục';
        foreach ( $terms as $term ) {
            $options_taxonomy[$term->term_id] = $term->name;
        }
	}

	
		// Pull all the tags into an array
	$options_tags = array();
	$options_tags_obj = get_tags();
	foreach ( $options_tags_obj as $tag ) {
		$options_tags[$tag->term_id] = $tag->name;
	}
		// Pull all the pages into an array
	$options_pages = array();
	$options_pages_obj = get_pages('sort_column=post_parent,menu_order');
	$options_pages[''] = 'Select a page:';
	foreach ($options_pages_obj as $page) {
		$options_pages[$page->ID] = $page->post_title;
	}

	$options = array();
	$imagepath =  get_template_directory_uri() . '/images/';

	
	$options[] = array(
		'name' => __('Tổng quan', 'cosmetic'),
		'type' => 'heading');
		$options[] = array(
		'desc' => __('
					<strong>Thông tin website</strong>
					<ul>
						<li><i>Giao diện đang sử dụng:</i> '.wp_get_theme()->get('Name').'</li>
						<li><i>Mô tả:</i> '.wp_get_theme()->get('Description').'</li>
						<li><i>Thiết kế bởi:</i> '.wp_get_theme()->get('Author').'</li>
						<li><i>Phiên bản:</i> '.wp_get_theme()->get('Version').'</li>
						<li><i>Phát triển nền tảng:</i> Wordpress '.get_bloginfo('version').'</li>
					</ul>

					', 'cosmetic'),
		'type' => 'info');
	/**
	 * Thiết lập thông tin website
	 */
	$options[] = array(
		'name' => __('Info Company', 'cosmetic'),
		'type' => 'heading');	

		$options[] = array(
		'name' => __('Logo', 'cosmetic'),
		'desc' => __('Upload your logo here', 'cosmetic'),
		'id' => 'logo',
		'class' => '',
		'std'	=> '',
		'type' => 'upload');

		$options[] = array(
		'name' => __('Giới thiệu ở footer', 'cosmetic'),
		'desc' => __('Đoạn giới thiệu', 'cosmetic'),
		'id'   => 'footer-info',
		'type' => 'textarea');

		$options[] = array(
			'name' => __('Ảnh khuyến mãi trong sản phẩm','cosmetic'),
			'id'   => 'anh-khuyen-mai',
			'type' => 'upload'
		);

	/**
	 * Thiết lập trang chủ
	 */
	$options[] = array(
		'name' => __('Home', 'cosmetic'),
		'type' => 'heading');	

		for ($x=1; $x <= 4; $x++) { 
			$options[] = array(
				'name' => __('Upload ảnh '.$x, 'cosmetic'),
				'desc' => __('Lựa chọn chuyên mục hiển thị ở Home', 'cosmetic'),
				'id' => 'select_img_'.$x,
				'type' => 'upload',
			);
			$options[] = array(
				'desc' => __('Tên mục', 'cosmetic'),
				'id' => 'select_title_'.$x,
				'std' => '',
				'class' => 'mini',
				'type' => 'text');
			$options[] = array(
				'desc' => __('Link của mục', 'cosmetic'),
				'id' => 'select_link_'.$x,
				'std' => '',
				'class' => 'mini',
				'type' => 'text');
			$options[] = array(
				'desc' => __('Nội dung của mục', 'cosmetic'),
				'id' => 'select_content_'.$x,
				'std' => '',
				'type' => 'textarea');
		}

		$options[] =array(
			'name' => __('Danh mục bán chạy', 'cosmetic'),
			'type' => 'info'
		);
		for($i=1;$i<=4;$i++){
	
			$options[] = array(
				'desc' => __('Link danh mục bán chạy '.$i, 'cosmetic'),
				'id' => 'select_best_taxonomy_'.$i,
				'type' => 'select',
			'options' => $options_taxonomy
			);
		}
	/**
	 * Thiết lập Popup
	 */
	$options[] = array(
		'name' => __('Modal', 'cosmetic'),
		'type' => 'heading');
		$options[] = array(
			'name' => __('Image', 'cosmetic'),
			'desc' => __('Upload your Image here', 'cosmetic'),
			'id' => 'popup_image',
			'class' => '',
			'std'	=> '',
			'type' => 'upload');
		$options[] = array(
			'name' => __('Set Minutes', 'cosmetic'),
			'desc' => __('Cài đặt sau bao nhiêu phút sẽ hiển thị Popup', 'cosmetic'),
			'id' => 'popup_minutes',
			'std' => '40',
			'type' => 'text',);

	/**
	 * Thiết lập mạng xã hội
	 */
	$options[] = array(
		'name' => __('Social Settings', 'cosmetic'),
		'type' => 'heading');
	
	$options[] = array(
		'desc' => __('Cài đặt mạng xã hội', 'cosmetic'),
		'type' => 'info');

	$options[] = array(
		'name' => __('Facebook', 'cosmetic'),
		'desc' => __('Địa chỉ Facebook', 'cosmetic'),
		'id' => 'facebook',
		'std' => 'https://www.facebook.com/cosmetic.net/?fref=ts',
		'class' => 'mini',
		'type' => 'text',
		'subtype' => 'url');
	
	$options[] = array(
		'name' => __('Instagram', 'cosmetic'),
		'desc' => __('Địa chỉ Instagram', 'cosmetic'),
		'id' => 'instagram',
		'std' => '',
		'class' => 'mini',
		'type' => 'text');
	
	$options[] = array(
		'name' => __('Skype', 'cosmetic'),
		'desc' => __('Nick Skype', 'cosmetic'),
		'id' => 'skype',
		'std' => '',
		'class' => 'mini',
		'type' => 'text',
		'subtype' => 'url');
		
	$options[] = array(
		'name' => __('Youtube', 'cosmetic'),
		'desc' => __('Địa chỉ Youtube', 'cosmetic'),
		'id' => 'youtube',
		'std' => '',
		'class' => 'mini',
		'type' => 'text',
		'subtype' => 'url');	
	/**
	 * Thiết lập liên hệ
	 */
	$options[] = array(
		'name' => __('Contact Details', 'cosmetic'),
		'type' => 'heading');
	
	$options[] = array(
		'name' => __('Image', 'cosmetic'),
		'desc' => __('Upload your Image here', 'cosmetic'),
		'id' => 'logo_footer',
		'class' => '',
		'std'	=> '',
		'type' => 'upload');
	$options[] = array(
		'desc' => __('Tên công ty', 'cosmetic'),
		'id' => 'cong-ty',
		'std' => 'Công Ty TNHH MTV Công Nghệ Và Truyền Thông cosmetic',
		'type' => 'text');	
		
	$options[] = array(
		'desc' => __('Địa chỉ', 'cosmetic'),
		'id' => 'dia-chi',
		'std' => ' 76 Hoàng Văn Thụ',
		'type' => 'text');	
				
	$options[] = array(
		'desc' => __('Điện thoại', 'cosmetic'),
		'id' => 'dien-thoai',
		'std' => '0982 802 531',
		'type' => 'text');

	$options[] = array(
		'desc' => __('Hotline', 'cosmetic'),
		'id' => 'hotline',
		'std' => '0982 802 531',
		'type' => 'text');

		
	$options[] = array(
		'desc' => __('Email', 'cosmetic'),
		'id' => 'email',
		'std' => sanitize_email( 'contact@cosmetic.net' ),
		'type' => 'text',
		'subtype' => 'email');	
	$options[] = array(
		'desc' => __('Website', 'cosmetic'),
		'id' => 'website',
		'std' => 'www.cosmetic.net',
		'type' => 'text',
		);
		
	return $options;
}