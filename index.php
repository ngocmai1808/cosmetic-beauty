<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package cosmetic
 */
get_header();
?>
	<div class="banner">
		<?php $args = array(
			'post_type' => 'banner',
			'order' => 'DESC', 
			'orderby' => 'date',
			'post_status' => 'publish'
		); 
		$the_query = new WP_Query( $args ); ?>
		<div class="owl-carousel owl-theme owl-banner">
			<?php if ( $the_query->have_posts() ) :
			while ( $the_query->have_posts() ) : $the_query->the_post();?>
			  	<div class="item">
			  		<?php the_post_thumbnail('full', ['class' => 'img-responsive responsive--full', 'title' => 'Feature image']);?>
			  	</div>
			<?php endwhile;
			endif; ?>
		</div>
		<?php wp_reset_postdata();?>
	</div>
	<section class="featured">
		<div class="container-fluid">
			<div class="row">
				<?php for($i=1;$i<=4;$i++){ ?>
					<div class="col-md-3 col-sm-6 col-xs-6">
						<div class="featured-item">
							<a class="featured-img" href="<?php echo of_get_option('select_link_'.$i,true); ?>">
								<img src="<?php echo of_get_option('select_img_'.$i); ?>" alt="anh" class="img-responsive">
								<div class="featured-effect"></div>
							</a>
							
							<p class="featured-title"><a class="featured-img" href="<?php echo of_get_option('select_link_'.$i,true); ?>"><?php echo of_get_option('select_title_'.$i); ?></a></p>
							<p><?php echo of_get_option('select_content_'.$i); ?></p>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
	</section>
	<section class="new">
		<div class="container-fluid">
			<h2 class="title-h2">
				<a href="/shop">Sản phẩm mới</a>
			</h2>
			<div class="short-desc">
				<span>Just in now</span>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="new-big">
						<img src="<?php echo get_template_directory_uri(); ?>/images/5.jpg" alt="cosmetic" class="img-responsive">
					</div>
				</div>
				<div class="col-md-6">
					<div class="new-sm">
						<div class="row">
						<?php $meta_query = WC()->query->get_meta_query();
							  $tax_query = WC()->query->get_tax_query();
							  $tax_query[] = array(
							  	'taxonomy' => 'product_visibility',
							  	'field' => 'name',
							  	'terms' => 'featured',
							  	'operator' => 'IN',
							  );
							  $args = array(
							  	'post_type' => 'product',
							  	'post_status' => 'publish',
							  	'posts_per_page' => 10,
							  	'meta_query' => $meta_query,
							  	'tax_query' => $tax_query,
							  );
							  $featured_query = new WP_Query($args);
							  if($featured_query->have_posts()){
							  	while($featured_query->have_posts()):
							  		$featured_query->the_post();
							  	
						 	?>
								<div class="col-md-4">
									<?php get_template_part('template-parts/content','product-home'); ?>
								</div>
							<?php endwhile; 
							}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="collect">
		<div class="container-fluid">
			<div class="collect-right">
				<img src="<?php echo get_template_directory_uri(); ?>/images/11.jpg" alt="anh" class="img-responsive">
			</div>
			<div class="collect-center">
				<img src="<?php echo get_template_directory_uri(); ?>/images/9.jpg" alt="anh" class="img-responsive">
				<img src="<?php echo get_template_directory_uri(); ?>/images/10.jpg" alt="anh" class="img-responsive">
				<img src="<?php echo get_template_directory_uri(); ?>/images/7.jpg" alt="anh" class="img-responsive">
				<img src="<?php echo get_template_directory_uri(); ?>/images/8.jpg" alt="anh" class="img-responsive">
			</div>
			<div class="collect-right">
				<img src="<?php echo get_template_directory_uri(); ?>/images/6.jpg" alt="anh" class="img-responsive">
			</div>
		</div>
	</section>
	<section class="best">
		<div class="container-fluid">
			<h2 class="title-h2">
				<a href="">Sản phẩm bán tốt nhất</a>
			</h2>
			<div class="short-desc">
				<span>Just Released</span>
			</div>
			<ul class="nav nav-tabs" id="myTab" role="tablist">
				<?php $best1 = get_term_by('id', of_get_option('select_best_taxonomy_1', true), 'product_cat'); 
				?>
			  <li class="nav-item">
			    <a class="nav-link active" id="best1-tab" data-toggle="tab" href="#best1" role="tab" aria-controls="best1" aria-selected="true"><?php echo $best1->name; ?></a>
			  </li>
			  <?php for($is=2; $is<=4;$is++){?>
			  	<?php $tho ='best' . $is; ?>
			  	<?php $tho = get_term_by('id', of_get_option('select_best_taxonomy_'.$is, true), 'product_cat'); ?>
 				<li class="nav-item">
			    	<a class="nav-link" id="best<?php echo $is; ?>-tab" data-toggle="tab" href="#best<?php echo $is; ?>" role="tab" aria-controls="best<?php echo $is; ?>" aria-selected="false"><?php echo $tho->name; ?></a>
			  	</li>
			   <?php } ?>
	
			</ul>
			<div class="tab-content">
			  <div class="tab-pane active" id="best1" role="tabpanel" aria-labelledby="best1-tab">
			  	<div class="row">
			  	<?php 
			  		$args = array(
			  			'post_type' => 'product',
			  			'post_status' => 'publish',
			  			'posts_per_page' => 6,
			  			'tax_query' => array(
			  				'relation' => 'AND',
			  				array(
			  					'taxonomy' => 'product_cat',
			  					'field' => 'slug',
			  					'terms' => $best1->slug,
			  					'include_children' => true,
			  					'operator' => 'IN'
			  				)
			  			)
			  		);
			  		$the_query = new WP_Query($args);
			  		if($the_query->have_posts()):
			  			while($the_query->have_posts()): $the_query->the_post();?>
			  				<div class="col-md-2 col-sm-3 col-xs-4">
								<?php get_template_part('template-parts/content','product-home'); ?>
							</div>
			  			<?php endwhile;
			  		endif;
			  		// Reset Post Data
						wp_reset_postdata();
				  	?>
				</div>
			  </div>
			  <?php for($j=2;$j<=4;$j++){?>
			  	<?php $thi = 'best' . $j; ?>
			  	<?php $thi = get_term_by('id', of_get_option('select_best_taxonomy_'.$j, true), 'product_cat');?>
					<div class="tab-pane" id="best<?php echo $j; ?>" role="tabpanel" aria-labelledby="best<?php echo $j; ?>-tab">
						<div class="row">
						  	<?php
						  		$args = array(
						  			'post_type' => 'product',
						  			'post_status' => 'publish',
						  			'posts_per_page' => 6,
						  			'tax_query' => array(
						  				'relation' => 'AND',
						  				array(
						  					'taxonomy' => 'product_cat',
						  					'field' => 'slug',
						  					'terms' => $thi->slug,
						  					'include_children' => true,
						  					'operator' => 'IN'
						  				)
						  			)
						  		);
						  		$the_query = new WP_Query($args);
						  		if($the_query->have_posts()):
						  			while($the_query->have_posts()): $the_query->the_post();?>
						  				<div class="col-md-2 col-sm-3 col-xs-4">
											<?php get_template_part('template-parts/content','product-home'); ?>
										</div>
						  			<?php endwhile;
						  		endif;
						  		// Reset Post Data
									wp_reset_postdata();
							  	?>
							</div>
					</div>
			   <?php } ?>
			 
			</div>
		</div>
	</section>	
	<?php dynamic_sidebar( 'home' ); ?>
	<section class="support">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-3">
					<div class="support-item">
						<i class="fa fa-truck" aria-hidden="true"></i>
						<p>Giao hành miễn phí</p>
						<p>Giao hàng trong vòng 5 ngày</p>
					</div>
				</div>
				<div class="col-md-3">
					<div class="support-item">
						<i class="fa fa-comments" aria-hidden="true"></i>
						<p>Hỗ trợ 24/7</p>
						<p>Hotline tư vấn <?php echo of_get_option('hotline'); ?></p>
					</div>
				</div>
				<div class="col-md-3">
					<div class="support-item">
						<i class="fa fa-retweet" aria-hidden="true"></i>
						<p>Chính sách đổi trả</p>
						<p>Không mất phí đổi trả</p>
					</div>
				</div>
				<div class="col-md-3">
					<div class="support-item">
						<i class="fa fa-money" aria-hidden="true"></i>
						<p>Tiết kiệm</p>
						<p>Những sản phẩm đặc biệt</p>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php
get_footer();?>