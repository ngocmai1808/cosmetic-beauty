<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package cosmetic
 */
?>
	</div><!-- #content -->
	<footer id="colophon" class="site-footer">
		<div class="footer-top">
			<div class="footer-left">
				<div class="container-fluid">
			
				<img src="<?php echo of_get_option('logo',true); ?>" alt="anh logo" class="img-responsive">
				<div class="footer-info">
					<?php echo of_get_option('footer-info', true); ?>
				</div>

				 <h3><?php echo __('Phương thức thanh toán') ?></h3>
				 <p>Hiện tại cửa hàng chúng tôi hỗ trợ nhiều hình thức thanh toán, giúp bạn chủ động và thuận tiện hơn trong quá trình giao hàng.</p>
				 <ul>
				 	<li>
				 		<img src="<?php echo get_template_directory_uri(); ?>/images/payment_icon.png" alt="anh logo" class="img-responsive">
				 	</li>
				 	<li>
				 		<img src="<?php echo get_template_directory_uri(); ?>/images/payment_icon_2.png" alt="anh logo" class="img-responsive">
				 	</li>
				 	<li>
				 		<img src="<?php echo get_template_directory_uri(); ?>/images/payment_icon_3.png" alt="anh logo" class="img-responsive">
				 	</li>
				 	<li>
				 		<img src="<?php echo get_template_directory_uri(); ?>/images/payment_icon_4.png" alt="anh logo" class="img-responsive">
				 	</li>
				 	<li>
				 		<img src="<?php echo get_template_directory_uri(); ?>/images/payment_icon_5.png" alt="anh logo" class="img-responsive">
				 	</li>
				 	<li>
				 		<img src="<?php echo get_template_directory_uri(); ?>/images/payment_icon_6.png" alt="anh logo" class="img-responsive">
				 	</li>
				 </ul>
			</div>
		</div>
		<div class="footer-right">
			<div class="container-fluid">

			
				<div class="row">
					<div class="col-md-4">
						<h4 class="title-h4">Liên hệ với chúng tôi</h4>
						<ul>
							<li><i class="fab fa-twitter"></i> Theo dõi trên Twitter</li>
							<li><i class="fab fa-facebook-f"></i> Theo dõi trên Facebook</li>
							<li><i class="fab fa-facebook-f"></i> Theo dõi trên Google</li>
							<li><i class="fab fa-instagram"></i> Theo dõi trên Instagram</li>
						</ul>
					</div>
					<div class="col-md-4">
						<h4 class="title-h4">Liên hệ với chúng tôi</h4>
						<ul>
							<li><i class="fab fa-twitter"></i> Theo dõi trên Twitter</li>
							<li><i class="fab fa-facebook-f"></i> Theo dõi trên Facebook</li>
							<li><i class="fab fa-facebook-f"></i> Theo dõi trên Google</li>
							<li><i class="fab fa-instagram"></i> Theo dõi trên Instagram</li>
						</ul>
					</div>
					<div class="col-md-4">
						<h4 class="title-h4">Liên hệ với chúng tôi</h4>
						<ul>
							<li><i class="fab fa-twitter"></i> Theo dõi trên Twitter</li>
							<li><i class="fab fa-facebook-f"></i> Theo dõi trên Facebook</li>
							<li><i class="fab fa-facebook-f"></i> Theo dõi trên Google</li>
							<li><i class="fab fa-instagram"></i> Theo dõi trên Instagram</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<h4 class="title-h4">Liên hệ với chúng tôi</h4>
						<ul>
							<li><i class="fab fa-twitter"></i> Theo dõi trên Twitter</li>
							<li><i class="fab fa-facebook-f"></i> Theo dõi trên Facebook</li>
							<li><i class="fab fa-facebook-f"></i> Theo dõi trên Google</li>
							<li><i class="fab fa-instagram"></i> Theo dõi trên Instagram</li>
						</ul>
					</div>
					<div class="col-md-8">
						<h4 class="title-h4">Connect with us</h4>
						<ul>
							<li>Địa chỉ: <?php echo of_get_option('dia-chi',true); ?></li>
							<li>Hotline: <?php echo of_get_option('hotline',true); ?></li>
							<li>Điện thoại: <?php echo of_get_option('dien-thoai',true); ?></li>
							<li>Email: <?php echo of_get_option('email',true); ?></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		</div>
		<div class="site-info">
			<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'cosmetic' ) ); ?>">
				<?php
				/* translators: %s: CMS name, i.e. WordPress. */
				printf( esc_html__( 'Proudly powered by %s', 'cosmetic' ), 'WordPress' );
				?>
			</a>
			<span class="sep"> | </span>
				<?php
				/* translators: 1: Theme name, 2: Theme author. */
				printf( esc_html__( 'Theme: %1$s by %2$s.', 'cosmetic' ), 'cosmetic', '<a href="">Mai Nguyen Ngoc</a>' );
				?>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->
<div class="back-to-top"><img src="<?php echo get_template_directory_uri(); ?>/images/top_to_btm.png" alt="back-to-top" class="img-responsive"></div>
<div class="phonering-alo-phone phonering-alo-green phonering-alo-show" id="phonering-alo-phoneIcon">
    <div class="phonering-alo-ph-circle"></div>
    <div class="phonering-alo-ph-circle-fill"></div>
    <div class="phonering-alo-ph-img-circle">
      <a href="tel:0369132997" class="pps-btn-img " title="Liên hệ">
        <img src="//i.imgur.com/v8TniL3.png" alt="Liên hệ" width="50" onmouseover="this.src='//i.imgur.com/v8TniL3.png';" onmouseout="this.src='//i.imgur.com/v8TniL3.png';">
      </a>
    </div>
  </div>
<div class="mobile-hotline" id="mobile-hotline">
    <a href="tel:0369132997" title="tel:0369132997">
      <span class="text-hotline">0369 132 997</span>
    </a>
  </div>
 

 <!-- Button social hot -->
<div class="list-social">
	<div class="list-social-icon">
		<div class="owl-theme owl-carousel owl-social">
			<div class="item">
				<img src="<?php echo get_template_directory_uri(); ?>/images/mess-fb.png" alt="" class="img-responsive">
			</div>
			<div class="item">
				<img src="<?php echo get_template_directory_uri(); ?>/images/skype.png" alt="" class="img-responsive">
			</div>
			<div class="item">
				<img src="<?php echo get_template_directory_uri(); ?>/images/zalo.png" alt="" class="img-responsive">
			</div>
			<div class="item">
				<img src="<?php echo get_template_directory_uri(); ?>/images/call.png" alt="" class="img-responsive">
			</div>
			<div class="item">
				<img src="<?php echo get_template_directory_uri(); ?>/images/mess.png" alt="" class="img-responsive">
			</div>
		</div>
	</div>
	<ul>
		<li>
			<img src="<?php echo get_template_directory_uri(); ?>/images/mess-fb.png" alt="" class="img-responsive">
			<span><a href="" target="_blank"></a>Messengers</span>
		</li>
		<li>
			<img src="<?php echo get_template_directory_uri(); ?>/images/skype.png" alt="" class="img-responsive">
			<span><a href="" target="_blank"></a>Skype</span>
		</li>
		<li>
			<img src="<?php echo get_template_directory_uri(); ?>/images/zalo.png" alt="" class="img-responsive">
			<span><a href="" target="_blank"></a>Zalo</span>
		</li>
		<li>
			<img src="<?php echo get_template_directory_uri(); ?>/images/call.png" alt="" class="img-responsive">
			<span><a href="<?php echo of_get_option('hotline',true); ?>" target="_blank">Hotline</a></span>
		</li>
		<li>
			<img src="<?php echo get_template_directory_uri(); ?>/images/mess.png" alt="" class="img-responsive">
			<span><a href="" target="_blank">Chat phone</a></span>
		</li>
	</ul>
</div>
<?php wp_footer(); ?>
</body>
</html>