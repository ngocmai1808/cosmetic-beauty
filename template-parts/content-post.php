<div class="post">
	<div class="post-img">
		<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'full' ); ?></a>
		<div class="radial-out"></div>
	</div>
	<div class="post-content">
		<div class="post-meta">
			<?php echo get_the_date( 'd/m/Y' ); ?>
		</div>
		<div class="post-title">
			<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
		</div>
		<div class="post-desc">
			<?php if(is_home()): 
				echo cosmetic_limit_words(get_the_excerpt(),'18'); 
			else :
				echo cosmetic_limit_words(get_the_excerpt(),'15'); 
			endif;?>
		</div>
		<div class="readmore"><a href="<?php the_permalink(); ?>"><?php echo __('Xem thêm','cosmetic'); ?></a></div>
	</div>
</div>