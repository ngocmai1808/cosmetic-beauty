<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package cosmetic
 */
global $product;
$price = $product->get_price_html();
?>

<div class="product">
	<div class="product-img">
		 <?php if($product->is_on_sale()) : ?>
		  <span class="label right label-info">
		    <?php  $percentage = round( ( ( $product->get_regular_price() - $product->get_sale_price() ) / $product->get_regular_price() ) * 100 );
		           echo sprintf( __('%s', 'woocommerce' ), $percentage . '%' ); ?>
		  </span>
		  <?php endif; ?>
		<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
			<?php if (has_post_thumbnail()){
				echo woocommerce_get_product_thumbnail();
			} else {
				echo '<img src=" '.get_bloginfo( 'stylesheet_directory').'/images/none.png" />';
			};?>
		</a>
		<div class="product-wrapper">
			<div class="product-wrapper-list">
				<a class="product-item-btn product-view" href="<?php the_permalink(); ?>"><span><i></i><i class="fa fa-eye" aria-hidden="true"></i></span></a>
		      	<?php echo apply_filters( 'woocommerce_loop_add_to_cart_link',
		        sprintf( '<a rel="nofollow" href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="%s"><i class="fa fa-cart-plus" aria-hidden="true"></i></a>',
		          esc_url( $product->add_to_cart_url() ),
		          esc_attr( isset( $quantity ) ? $quantity : 1 ),
		          esc_attr( $product->id ),
		          esc_attr( $product->get_sku() ),
		          esc_attr( isset( $class ) ? $class : 'button product_type_simple add_to_cart_button ajax_add_to_cart product--order product-item-btn'),
		          esc_html( $product->add_to_cart_text() )
		        ),
		      $product );?>
		      <span><i class="fa fa-heart" aria-hidden="true"></i></span>
			</div>

		</div>
	</div>
	<div class="product-title">
		<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
	</div>
	<div class="product-price">
		<?php if ($price): ?>
			<?php echo $price; ?>
		<?php else: echo "Liên hệ";endif; ?>
	</div>
	<div class="product-star">
		<i class="fa fa-star" aria-hidden="true"></i>
		<i class="fa fa-star" aria-hidden="true"></i>
		<i class="fa fa-star" aria-hidden="true"></i>
		<i class="fa fa-star" aria-hidden="true"></i>
		<i class="fa fa-star" aria-hidden="true"></i>
	</div>
</div>