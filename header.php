<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package cosmetic
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">


	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<div id="page" class="site">
		<header id="masthead" class="site-header">
			<div class="header-top">
				<div class="container">
					<div class="header-top-left">
						<ul> 
							<li>
								<i class="fas fa-phone-volume"></i><span><?php echo of_get_option('hotline',true); ?></span>
							</li>
							<li>
								<i class="far fa-envelope"></i><span><?php echo of_get_option('email',true); ?></span>
							</li>
						</ul>
					</div>
					<div class="header-top-right hidden-xs">
						<?php
						wp_nav_menu( array(
							'theme_location' => 'menu-top',
							'menu_id'        => 'top-menu',
						) );
						?>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="site-branding">
					<div class="pull-left">
						<?php
						the_custom_logo();
						if ( is_front_page() && is_home() ) :
							?>
						<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
						<?php
					else :
						?>
						<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
						<?php
					endif;
					$cosmetic_description = get_bloginfo( 'description', 'display' );
					if ( $cosmetic_description || is_customize_preview() ) :
						?>
						<p class="site-description"><?php echo $cosmetic_description;?></p>
					<?php endif; ?>
				</div>
				<div class="pull-right">
					<a href="<?php echo get_permalink( get_page_by_path('checkout')); ?>" class="btn-check-order">
						<i class="fa fa-truck"></i> 
						<span>Đơn hàng</span></a>
						
						<a href="<?php echo get_permalink( get_page_by_path('cart')); ?>" class="btn-cart">
							<i class="fa fa-shopping-cart"></i> 
							Giỏ hàng 
							<span class="cart-count"></span>
						</a>
						
					</div>
				</div><!-- .site-branding -->
			</div><div class="clearfix"></div>
			<nav id="site-navigation" class="main-navigation">
				<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'cosmetic' ); ?></button>
				<?php
				wp_nav_menu( array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
					'menu_class'     => 'container'
				) );
				?>
			</nav><!-- #site-navigation -->
			
		</header><!-- #masthead -->
		
		<div id="content" class="site-content">