<?php /* Template Name: Magazine*/ ?>
<?php get_header(); ?>
<div class="page-banner">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="page-banner-big">
				<?php $args = array(
					'post_type' => 'product',
					'post_status' => 'publish',
					'posts_per_page' => 1,
					'tax_query' => array(                     
				    'relation' => 'AND',                      
				      array(
				        'taxonomy' => 'product_cat',
				        'field' => 'id',                    
				        'terms' => of_get_option('select_best_taxonomy_1'),    
				        'include_children' => true,           
				        'operator' => 'IN'      ,              
				      ),
					'order' => 'DESC', 
					'orderby' => 'date')
				); 
				$the_query = new WP_Query($args);
				if($the_query->have_posts()):
					while ( $the_query->have_posts() ) :
						$the_query->the_post();?>
						<div class="page-banner-item">
							<a href="<?php the_permalink(); ?>" rel="prettyPhoto">
								<img src="<?php echo get_template_directory_uri(); ?>/images/7.jpg" alt="anh" class="img-responsive">
							</a>
							<div class="page-banner-content">
								<?php $term = get_term(of_get_option('select_best_taxonomy_1')); ?>
								<h3 class="page-banner-cat">
									<span><?php echo $term->name; ?></span>
								</h3>
								<div class="page-banner-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
								<div class="page-banner-date">
									<?php the_date('d/m/Y'); ?>
								</div>
							</div>
						</div>
					<?php endwhile;
				endif;
				wp_reset_postdata();
				?>
				</div>
			</div>
			<div class="col-md-6">
				<div class="page-banner-medium">
				<?php $args = array(
					'post_type' => 'product',
					'post_status' => 'publish',
					'posts_per_page' => 1,
					'tax_query' => array(                     
				    'relation' => 'AND',                     
				      array(
				        'taxonomy' => 'product_cat',              
				        'field' => 'id',                   
				        'terms' => of_get_option('select_best_taxonomy_1'),   
				        'include_children' => true,           
				        'operator' => 'IN'      ,             
				      ),
					'order' => 'DESC', 
					'orderby' => 'date')
				); 
				$the_query = new WP_Query($args);
				if($the_query->have_posts()):
					while ( $the_query->have_posts() ) :
						$the_query->the_post();?>
						<div class="page-banner-item">
							<a href="<?php the_permalink(); ?>" rel="prettyPhoto">
								<img src="<?php echo get_template_directory_uri(); ?>/images/7_2.jpg" alt="anh" class="img-responsive">
							</a>
							<div class="page-banner-content">
								<?php $term = get_term(of_get_option('select_best_taxonomy_1')); ?>
								<h3 class="page-banner-cat">
									<span><?php echo $term->name; ?></span>
								</h3>
								<div class="page-banner-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
							</div>
						</div>
					<?php endwhile;
				endif;
				wp_reset_postdata();
				?>
				</div>
				<div class="page-banner-sm">
					<?php $args = array(
					'post_type' => 'product',
					'post_status' => 'publish',
					'posts_per_page' => 2,
					'tax_query' => array(                     
				    'relation' => 'AND',                      
				      array(
				        'taxonomy' => 'product_cat',              
				        'field' => 'id',                   
				        'terms' => of_get_option('select_best_taxonomy_1'),   
				        'include_children' => true,          
				        'operator' => 'IN',              
				      ),
					'order' => 'DESC', 
					'orderby' => 'date')
				); 
				$the_query = new WP_Query($args);
				if($the_query->have_posts()):
					while ( $the_query->have_posts() ) :
						$the_query->the_post();?>
						<div class="col-md-6">
							<div class="page-banner-item">
								<a href="<?php the_permalink(); ?>" rel="prettyPhoto">
									<img src="<?php echo get_template_directory_uri(); ?>/images/7.jpg" alt="anh" class="img-responsive">
								</a>
								<div class="page-banner-content">
									<?php $term = get_term(of_get_option('select_best_taxonomy_1')); ?>
									<h3 class="page-banner-cat">
										<span><?php echo $term->name; ?></span>
									</h3>
									<div class="page-banner-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
								</div>
							</div>
						</div>
					<?php endwhile;
				endif;
				wp_reset_postdata();
				?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="main-content">
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<div class="new">
					<div class="new-title">
						<?php $args = array(
							'post_type' => 'product',
							'post_status' => 'publish',
							'posts_per_page' => 1,
							'order' => 'DESC', 
							'orderby' => 'date',
							'tax_query' => array(                    
						    'relation' => 'AND',              
						      array(
						        'taxonomy' => 'product_cat',               
						        'field' => 'id',                    
						        'terms' => of_get_option('select_best_taxonomy_1'),    
						        'include_children' => true,           
						        'operator' => 'IN',             
						      ),
							)
						); 
						$term = get_term(of_get_option('select_best_taxonomy_1'));
						?>
						<h2 class="title-h2"><?php echo $term->name; ?></h2>
						<?php
						$term_id = 16;
						$taxonomy_name = 'product_cat';
						$term_children = get_term_children( $term_id, $taxonomy_name );
						echo '<ul>';
						foreach ( $term_children as $child ) {
							$term = get_term_by( 'id', $child, $taxonomy_name );
							echo '<li><a href="' . get_term_link( $child, $taxonomy_name ) . '">' . $term->name . '</a></li>';
						}
						echo '</ul>';
						?> 
					</div>
					<div class="new-content">
						<div class="row">
							<div class="col-md-6">
								<?php 
									$the_query = new WP_Query( $args );
									// The Loop
									if ( $the_query->have_posts() ) :
									while ( $the_query->have_posts() ) : $the_query->the_post();?>
										<div class="new-item">
											<a href="<?php the_permalink(); ?>" rel="prettyPhoto">
												<img src="<?php echo get_template_directory_uri(); ?>/images/7.jpg" alt="anh" class="img-responsive">
											</a>
											<div class="new-content">
												<div class="new-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
												<div class="new-date">
													<?php echo get_the_date(); ?>
												</div>
												<div class="new-expert">
													Prince Edward Island, Canada Prince Edward Island is a Canadian province. It is one of the three Maritime provinces and it is the smallest in...
												</div>			
											</div>
										</div>
									<?php endwhile;
									endif;
									// Reset Post Data
									wp_reset_postdata();
									?>
							</div>
							<div class="col-md-6">
								<?php $args = array(
									'post_type' => 'product',
									'post_status' => 'publish',
									'posts_per_page' => 5,
									'order' => 'DESC', 
									'orderby' => 'date',
									'tax_query' => array(                     
								    'relation' => 'AND',                      
								      array(
								        'taxonomy' => 'product_cat',                
								        'field' => 'id',                   
								        'terms' => of_get_option('select_best_taxonomy_1'),    
								        'include_children' => true,           
								        'operator' => 'IN',             
								      ),
									)
								); 
									$the_query = new WP_Query( $args );
									// The Loop
									if ( $the_query->have_posts() ) :
									while ( $the_query->have_posts() ) : $the_query->the_post();?>
										<div class="new-item">
											<div class="row">
												<div class="col-md-3">
													<a href="<?php the_permalink(); ?>" rel="prettyPhoto">
														<img src="<?php echo get_template_directory_uri(); ?>/images/7.jpg" alt="anh" class="img-responsive">
													</a>
												</div>
												<div class="col-md-9">
													<div class="new-content">
														<div class="new-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
														<div class="new-date">
															<?php echo get_the_date(); ?>
														</div>
													</div>
												</div>
											</div>
											</div>
									<?php endwhile;
									endif;
									// Reset Post Data
									wp_reset_postdata();
									?>
								</div>
							</div>
						</div>
					</div>
				<div class="new">
					<div class="new-title">
						<?php $args = array(
							'post_type' => 'product',
							'post_status' => 'publish',
							'posts_per_page' => 2,
							'order' => 'DESC', 
							'orderby' => 'date',
							'tax_query' => array(                     //(array) - Lấy bài viết dựa theo taxonomy
						    'relation' => 'AND',                      //(string) - Mối quan hệ giữa các tham số bên trong, AND hoặc OR
						      array(
						        'taxonomy' => 'product_cat',                //(string) - Tên của taxonomy
						        'field' => 'id',                    //(string) - Loại field cần xác định term của taxonomy, sử dụng 'id' hoặc 'slug'
						        'terms' => of_get_option('select_best_taxonomy_1'),    //(int/string/array) - Slug của các terms bên trong taxonomy cần lấy bài
						        'include_children' => true,           //(bool) - Lấy category con, true hoặc false
						        'operator' => 'IN'      ,              //(string) - Toán tử áp dụng cho mảng tham số này. Sử dụng 'IN' hoặc 'NOT IN'
						      ),
							)
						); 
						$term = get_term(of_get_option('select_best_taxonomy_1'));
						?>
						<h2 class="title-h2"><?php echo $term->name; ?></h2>
						<?php
						$term_id = 16;
						$taxonomy_name = 'product_cat';
						$term_children = get_term_children( $term_id, $taxonomy_name );
						echo '<ul>';
						foreach ( $term_children as $child ) {
							$term = get_term_by( 'id', $child, $taxonomy_name );
							echo '<li><a href="' . get_term_link( $child, $taxonomy_name ) . '">' . $term->name . '</a></li>';
						}
						echo '</ul>';
						?> 
					</div>
					<div class="new-content">
						<div class="row">
							<div class="col-md-12">
								<?php 
									$the_query = new WP_Query( $args );
									// The Loop
									if ( $the_query->have_posts() ) :
									while ( $the_query->have_posts() ) : $the_query->the_post();?>
										<div class="col-md-6">
											<div class="new-item">
												<a href="<?php the_permalink(); ?>" rel="prettyPhoto">
													<img src="<?php echo get_template_directory_uri(); ?>/images/7.jpg" alt="anh" class="img-responsive">
												</a>
												<div class="new-content">
													<div class="new-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
													<div class="new-date">
														<?php echo get_the_date(); ?>
													</div>
													<div class="new-expert">
														Prince Edward Island, Canada Prince Edward Island is a Canadian province. It is one of the three Maritime provinces and it is the smallest in...
													</div>			
												</div>
											</div>
										</div>
									<?php endwhile;
									endif;
									// Reset Post Data
									wp_reset_postdata();
									?>
								</div>
								<div class="col-md-12">
								<?php $args = array(
									'post_type' => 'product',
									'post_status' => 'publish',
									'posts_per_page' => 4,
									'order' => 'DESC', 
									'orderby' => 'date',
									'tax_query' => array(                     //(array) - Lấy bài viết dựa theo taxonomy
								    'relation' => 'AND',                      //(string) - Mối quan hệ giữa các tham số bên trong, AND hoặc OR
								      array(
								        'taxonomy' => 'product_cat',                //(string) - Tên của taxonomy
								        'field' => 'id',                    //(string) - Loại field cần xác định term của taxonomy, sử dụng 'id' hoặc 'slug'
								        'terms' => of_get_option('select_best_taxonomy_1'),    //(int/string/array) - Slug của các terms bên trong taxonomy cần lấy bài
								        'include_children' => true,           //(bool) - Lấy category con, true hoặc false
								        'operator' => 'IN'      ,              //(string) - Toán tử áp dụng cho mảng tham số này. Sử dụng 'IN' hoặc 'NOT IN'
								      ),
									)
								); 
									$the_query = new WP_Query( $args );
									// The Loop
									if ( $the_query->have_posts() ) :
									while ( $the_query->have_posts() ) : $the_query->the_post();?>
										<div class="col-md-6">
											<div class="new-item">
												<div class="row">
													<div class="col-md-3">
														<a href="<?php the_permalink(); ?>" rel="prettyPhoto">
															<img src="<?php echo get_template_directory_uri(); ?>/images/7.jpg" alt="anh" class="img-responsive">
														</a>
													</div>
													<div class="col-md-9">
														<div class="new-content">
															<div class="new-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
															<div class="new-date">
																<?php echo get_the_date(); ?>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									<?php endwhile;
									endif;
									// Reset Post Data
									wp_reset_postdata();
									?>
							</div>
							</div>
						</div>
					</div>
				<div class="new">
					<div class="new-title">
						<?php $args = array(
							'post_type' => 'product',
							'post_status' => 'publish',
							'posts_per_page' => 3,
							'order' => 'DESC', 
							'orderby' => 'date',
							'tax_query' => array(                     //(array) - Lấy bài viết dựa theo taxonomy
						    'relation' => 'AND',                      //(string) - Mối quan hệ giữa các tham số bên trong, AND hoặc OR
						      array(
						        'taxonomy' => 'product_cat',                //(string) - Tên của taxonomy
						        'field' => 'id',                    //(string) - Loại field cần xác định term của taxonomy, sử dụng 'id' hoặc 'slug'
						        'terms' => of_get_option('select_best_taxonomy_1'),    //(int/string/array) - Slug của các terms bên trong taxonomy cần lấy bài
						        'include_children' => true,           //(bool) - Lấy category con, true hoặc false
						        'operator' => 'IN'      ,              //(string) - Toán tử áp dụng cho mảng tham số này. Sử dụng 'IN' hoặc 'NOT IN'
						      ),
							)
						); 
						$term = get_term(of_get_option('select_best_taxonomy_1'));
						?>
						<h2 class="title-h2"><?php echo $term->name; ?></h2>
						<?php
						$term_id = 16;
						$taxonomy_name = 'product_cat';
						$term_children = get_term_children( $term_id, $taxonomy_name );
						echo '<ul>';
						foreach ( $term_children as $child ) {
							$term = get_term_by( 'id', $child, $taxonomy_name );
							echo '<li><a href="' . get_term_link( $child, $taxonomy_name ) . '">' . $term->name . '</a></li>';
						}
						echo '</ul>';
						?> 
					</div>
					<div class="new-content">
						<div class="row">
							<div class="col-md-12">
								<?php 
									$the_query = new WP_Query( $args );
									// The Loop
									if ( $the_query->have_posts() ) :
									while ( $the_query->have_posts() ) : $the_query->the_post();?>
										<div class="col-md-4">
											<div class="new-item">
												<a href="<?php the_permalink(); ?>" rel="prettyPhoto">
													<img src="<?php echo get_template_directory_uri(); ?>/images/7.jpg" alt="anh" class="img-responsive">
												</a>
												<div class="new-content">
													<div class="new-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
													<div class="new-date">
														<?php echo get_the_date(); ?>
													</div>
												
												</div>
											</div>
										</div>
									<?php endwhile;
									endif;
									// Reset Post Data
									wp_reset_postdata();
									?>
								</div>
					
							</div>
						</div>
					</div>
				<div class="new">
					<div class="new-title">
						<?php $args = array(
							'post_type' => 'product',
							'post_status' => 'publish',
							'posts_per_page' => 5,
							'order' => 'DESC', 
							'orderby' => 'date',
							'tax_query' => array(                     //(array) - Lấy bài viết dựa theo taxonomy
						    'relation' => 'AND',                      //(string) - Mối quan hệ giữa các tham số bên trong, AND hoặc OR
						      array(
						        'taxonomy' => 'product_cat',                //(string) - Tên của taxonomy
						        'field' => 'id',                    //(string) - Loại field cần xác định term của taxonomy, sử dụng 'id' hoặc 'slug'
						        'terms' => of_get_option('select_best_taxonomy_1'),    //(int/string/array) - Slug của các terms bên trong taxonomy cần lấy bài
						        'include_children' => true,           //(bool) - Lấy category con, true hoặc false
						        'operator' => 'IN'      ,              //(string) - Toán tử áp dụng cho mảng tham số này. Sử dụng 'IN' hoặc 'NOT IN'
						      ),
							)
						); 
						$term = get_term(of_get_option('select_best_taxonomy_1'));
						?>
						<h2 class="title-h2"><?php echo $term->name; ?></h2>
						<?php
						$term_id = 16;
						$taxonomy_name = 'product_cat';
						$term_children = get_term_children( $term_id, $taxonomy_name );
						echo '<ul>';
						foreach ( $term_children as $child ) {
							$term = get_term_by( 'id', $child, $taxonomy_name );
							echo '<li><a href="' . get_term_link( $child, $taxonomy_name ) . '">' . $term->name . '</a></li>';
						}
						echo '</ul>';
						?> 
					</div>
					<div class="new-content">
						<div class="row">
							<div class="col-md-12">
								<?php 
									$the_query = new WP_Query( $args );
									// The Loop
									if ( $the_query->have_posts() ) :
									while ( $the_query->have_posts() ) : $the_query->the_post();?>
										<div class="new-item">
											<div class="row">
												<div class="col-md-3">
													<a href="<?php the_permalink(); ?>" rel="prettyPhoto">
														<img src="<?php echo get_template_directory_uri(); ?>/images/7.jpg" alt="anh" class="img-responsive">
													</a>
												</div>
												<div class="col-md-9">
													<div class="new-content">
														<div class="new-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
														<div class="new-date">
															<?php echo get_the_date(); ?>
														</div>
														<div class="new-expert">
															Prince Edward Island, Canada Prince Edward Island is a Canadian province. It is one of the three Maritime provinces and it is the smallest in...
														</div>			
													</div>
												</div>
											</div>
											
											
										</div>
										
									<?php endwhile;
									endif;
									// Reset Post Data
									wp_reset_postdata();
									?>
								</div>
							</div>
						</div>
					</div>
				<div class="col-md-6">
					<div class="new">
					<div class="new-title">
						<?php $args = array(
							'post_type' => 'product',
							'post_status' => 'publish',
							'posts_per_page' => 1,
							'order' => 'DESC', 
							'orderby' => 'date',
							'tax_query' => array(                     //(array) - Lấy bài viết dựa theo taxonomy
						    'relation' => 'AND',                      //(string) - Mối quan hệ giữa các tham số bên trong, AND hoặc OR
						      array(
						        'taxonomy' => 'product_cat',                //(string) - Tên của taxonomy
						        'field' => 'id',                    //(string) - Loại field cần xác định term của taxonomy, sử dụng 'id' hoặc 'slug'
						        'terms' => of_get_option('select_best_taxonomy_1'),    //(int/string/array) - Slug của các terms bên trong taxonomy cần lấy bài
						        'include_children' => true,           //(bool) - Lấy category con, true hoặc false
						        'operator' => 'IN'      ,              //(string) - Toán tử áp dụng cho mảng tham số này. Sử dụng 'IN' hoặc 'NOT IN'
						      ),
							)
						); 
						$term = get_term(of_get_option('select_best_taxonomy_1'));
						?>
						<h2 class="title-h2"><?php echo $term->name; ?></h2>

					</div>
					<div class="new-content">
						<div class="row">
							<div class="col-md-12">
								<?php 
									$the_query = new WP_Query( $args );
									// The Loop
									if ( $the_query->have_posts() ) :
									while ( $the_query->have_posts() ) : $the_query->the_post();?>
											<div class="new-item">
												<a href="<?php the_permalink(); ?>" rel="prettyPhoto">
													<img src="<?php echo get_template_directory_uri(); ?>/images/7.jpg" alt="anh" class="img-responsive">
												</a>
												<div class="new-content">
													<div class="new-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
													<div class="new-date">
														<?php echo get_the_date(); ?>
													</div>
													<div class="new-expert">
														Prince Edward Island, Canada Prince Edward Island is a Canadian province. It is one of the three Maritime provinces and it is the smallest in...
													</div>			
												</div>
											</div>
									<?php endwhile;
									endif;
									// Reset Post Data
									wp_reset_postdata();
									?>
								</div>
								<div class="col-md-12">
								<?php $args = array(
									'post_type' => 'product',
									'post_status' => 'publish',
									'posts_per_page' => 2,
									'order' => 'DESC', 
									'orderby' => 'date',
									'tax_query' => array(                     //(array) - Lấy bài viết dựa theo taxonomy
								    'relation' => 'AND',                      //(string) - Mối quan hệ giữa các tham số bên trong, AND hoặc OR
								      array(
								        'taxonomy' => 'product_cat',                //(string) - Tên của taxonomy
								        'field' => 'id',                    //(string) - Loại field cần xác định term của taxonomy, sử dụng 'id' hoặc 'slug'
								        'terms' => of_get_option('select_best_taxonomy_1'),    //(int/string/array) - Slug của các terms bên trong taxonomy cần lấy bài
								        'include_children' => true,           //(bool) - Lấy category con, true hoặc false
								        'operator' => 'IN'      ,              //(string) - Toán tử áp dụng cho mảng tham số này. Sử dụng 'IN' hoặc 'NOT IN'
								      ),
									)
								); 
									$the_query = new WP_Query( $args );
									// The Loop
									if ( $the_query->have_posts() ) :
									while ( $the_query->have_posts() ) : $the_query->the_post();?>
											<div class="new-item">
												<div class="row">
													<div class="col-md-3">
														<a href="<?php the_permalink(); ?>" rel="prettyPhoto">
															<img src="<?php echo get_template_directory_uri(); ?>/images/7.jpg" alt="anh" class="img-responsive">
														</a>
													</div>
													<div class="col-md-9">
														<div class="new-content">
															<div class="new-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
															<div class="new-date">
																<?php echo get_the_date(); ?>
															</div>
														</div>
													</div>
												</div>
											</div>
									<?php endwhile;
									endif;
									// Reset Post Data
									wp_reset_postdata();
									?>
							</div>
							</div>
						</div>
					</div>

				</div>
				<div class="col-md-6">
					<div class="new">
					<div class="new-title">
						<?php $args = array(
							'post_type' => 'product',
							'post_status' => 'publish',
							'posts_per_page' => 1,
							'order' => 'DESC', 
							'orderby' => 'date',
							'tax_query' => array(                     //(array) - Lấy bài viết dựa theo taxonomy
						    'relation' => 'AND',                      //(string) - Mối quan hệ giữa các tham số bên trong, AND hoặc OR
						      array(
						        'taxonomy' => 'product_cat',                //(string) - Tên của taxonomy
						        'field' => 'id',                    //(string) - Loại field cần xác định term của taxonomy, sử dụng 'id' hoặc 'slug'
						        'terms' => of_get_option('select_best_taxonomy_1'),    //(int/string/array) - Slug của các terms bên trong taxonomy cần lấy bài
						        'include_children' => true,           //(bool) - Lấy category con, true hoặc false
						        'operator' => 'IN'      ,              //(string) - Toán tử áp dụng cho mảng tham số này. Sử dụng 'IN' hoặc 'NOT IN'
						      ),
							)
						); 
						$term = get_term(of_get_option('select_best_taxonomy_1'));
						?>
						<h2 class="title-h2"><?php echo $term->name; ?></h2>

					</div>
					<div class="new-content">
						<div class="row">
							<div class="col-md-12">
								<?php 
									$the_query = new WP_Query( $args );
									// The Loop
									if ( $the_query->have_posts() ) :
									while ( $the_query->have_posts() ) : $the_query->the_post();?>
											<div class="new-item">
												<a href="<?php the_permalink(); ?>" rel="prettyPhoto">
													<img src="<?php echo get_template_directory_uri(); ?>/images/7.jpg" alt="anh" class="img-responsive">
												</a>
												<div class="new-content">
													<div class="new-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
													<div class="new-date">
														<?php echo get_the_date(); ?>
													</div>
													<div class="new-expert">
														Prince Edward Island, Canada Prince Edward Island is a Canadian province. It is one of the three Maritime provinces and it is the smallest in...
													</div>			
												</div>
											</div>
									<?php endwhile;
									endif;
									// Reset Post Data
									wp_reset_postdata();
									?>
								</div>
								<div class="col-md-12">
								<?php $args = array(
									'post_type' => 'product',
									'post_status' => 'publish',
									'posts_per_page' => 2,
									'order' => 'DESC', 
									'orderby' => 'date',
									'tax_query' => array(                     //(array) - Lấy bài viết dựa theo taxonomy
								    'relation' => 'AND',                      //(string) - Mối quan hệ giữa các tham số bên trong, AND hoặc OR
								      array(
								        'taxonomy' => 'product_cat',                //(string) - Tên của taxonomy
								        'field' => 'id',                    //(string) - Loại field cần xác định term của taxonomy, sử dụng 'id' hoặc 'slug'
								        'terms' => of_get_option('select_best_taxonomy_1'),    //(int/string/array) - Slug của các terms bên trong taxonomy cần lấy bài
								        'include_children' => true,           //(bool) - Lấy category con, true hoặc false
								        'operator' => 'IN'      ,              //(string) - Toán tử áp dụng cho mảng tham số này. Sử dụng 'IN' hoặc 'NOT IN'
								      ),
									)
								); 
									$the_query = new WP_Query( $args );
									// The Loop
									if ( $the_query->have_posts() ) :
									while ( $the_query->have_posts() ) : $the_query->the_post();?>
											<div class="new-item">
												<div class="row">
													<div class="col-md-3">
														<a href="<?php the_permalink(); ?>" rel="prettyPhoto">
															<img src="<?php echo get_template_directory_uri(); ?>/images/7.jpg" alt="anh" class="img-responsive">
														</a>
													</div>
													<div class="col-md-9">
														<div class="new-content">
															<div class="new-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
															<div class="new-date">
																<?php echo get_the_date(); ?>
															</div>
														</div>
													</div>
												</div>
											</div>
									<?php endwhile;
									endif;
									// Reset Post Data
									wp_reset_postdata();
									?>
							</div>
							</div>
						</div>
					</div>
				
				</div>

				
			</div>
			<div class="col-md-3">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>