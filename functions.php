<?php
/**
 * cosmetic functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package cosmetic
 */
include('lib/includes/widget_bai_viet_moi_nhat_theo_dm.php');
include('lib/includes/widget_bai_viet_moi_nhat_sidebar.php');
include('lib/includes/widget_san_pham_moi_nhat_theo_dm.php');
include('lib/includes/popular-tags-comments.php');
define ('THEME_NAME','COSMETIC');
define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_stylesheet_directory_uri() . '/inc/admin/' );
require_once dirname( __FILE__ ) . '/inc/admin/options-framework.php';
$optionsfile = locate_template( 'options.php' );
load_template( $optionsfile );
  add_theme_support( 'post-thumbnails' );
        add_image_size( 'product', 180, 225, true );

if ( ! function_exists( 'cosmetic_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function cosmetic_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on cosmetic, use a find and replace
		 * to change 'cosmetic' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'cosmetic', get_template_directory() . '/languages' );
		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );
		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );
		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'cosmetic' ),
			'menu-top' => esc_html__( 'Menu Top', 'cosmetic' ),
		) );
		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );
		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'cosmetic_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );
		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );
		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'cosmetic_setup' );
/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function cosmetic_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'cosmetic_content_width', 640 );
}
add_action( 'after_setup_theme', 'cosmetic_content_width', 0 );
/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function cosmetic_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar Woo', 'cosmetic' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'cosmetic' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'cosmetic_widgets_init' );
/**
 * Creates a sidebar
 * @param string|array  Builds Sidebar based off of 'name' and 'id' values.
 */
register_sidebar(array(
	'name'          => __( 'Trang chủ', 'cosmetic' ),
	'id'            => 'home',
	'description'   => '',
	'class'         => '',
	'before_widget' => '<section class="widget-home"><div class="container-fluid">',
	'after_widget'  => '</div></section>',
	'before_title'  => '<h2 class="title-h2">',
	'after_title'   => '</h2>',
	)
);
register_sidebar(array(
	'name'          => __( 'Sidebar Post', 'cosmetic' ),
	'id'            => 'sidebar',
	'description'   => esc_html__( 'Add widgets here.', 'cosmetic' ),
	'class'         => '',
	'before_widget' => '<section id="%1$s" class="widget %2$s">',
	'after_widget'  => '</section>',
	'before_title'  => '<h3 class="widget-title">',
	'after_title'   => '</h3>',
	)
);
/**
 * Enqueue scripts and styles.
 */
function cosmetic_scripts() {
	wp_enqueue_style('animate', get_template_directory_uri() . '/lib/css/animate.css', array(), '' );
	wp_enqueue_style('owl-min', get_template_directory_uri() . '/lib/css/owl.carousel.min.css', array(), '' );
	wp_enqueue_style('owl-default', get_template_directory_uri() . '/lib/css/owl.theme.default.min.css', array(), '' );
	wp_enqueue_style('bootstrap-min', get_template_directory_uri() . '/lib/css/bootstrap.min.css', array(), '' );
	wp_enqueue_style( 'cosmetic-style', get_stylesheet_uri() );
	wp_enqueue_script( 'cosmetic-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	wp_enqueue_script( 'jquery-js', get_template_directory_uri() . '/lib/js/jquery.min.js', array(), '20151215', true );
	wp_enqueue_script( 'owl-min-js', get_template_directory_uri() . '/lib/js/owl.carousel.min.js', array(), '20151215', true );
	wp_enqueue_script( 'iso-js', get_template_directory_uri() . '/lib/js/isotope.pkgd.min.js', array(), '20151215', true );
	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/lib/js/bootstrap.min.js', array(), '20151215', true );
	wp_enqueue_script( 'custom-js', get_template_directory_uri() . '/lib/js/custom.js', array(), '20151215', true );
	wp_enqueue_script( 'cosmetic-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'cosmetic_scripts' );
/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';
/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';
/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';
/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';
/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}
/**
 * Registers a new post type
 * @uses $wp_post_types Inserts new post type object into the list
 *
 * @param string  Post type key, must not exceed 20 characters
 * @param array|string  See optional args description above.
 * @return object|WP_Error the registered post type object, or an error object
 */
function banner() {
	$labels = array(
		'name'               => __( 'Banner', 'cosmetic' ),
		'singular_name'      => __( 'Banner', 'cosmetic' ),
		'add_new'            => _x( 'Add New Banner', 'cosmetic', 'cosmetic' ),
		'add_new_item'       => __( 'Add New Banner', 'cosmetic' ),
		'edit_item'          => __( 'Edit Banner', 'cosmetic' ),
		'new_item'           => __( 'New Banner', 'cosmetic' ),
		'view_item'          => __( 'View Banner', 'cosmetic' ),
		'search_items'       => __( 'Search Banner', 'cosmetic' ),
		'not_found'          => __( 'No Banner found', 'cosmetic' ),
		'not_found_in_trash' => __( 'No Banner found in Trash', 'cosmetic' ),
		'parent_item_colon'  => __( 'Parent Banner:', 'cosmetic' ),
		'menu_name'          => __( 'Banner', 'cosmetic' ),
	);
	$args = array(
		'labels'              => $labels,
		'hierarchical'        => false,
		'description'         => 'description',
		'taxonomies'          => array(),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => null,
		'menu_icon'           => null,
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => true,
		'capability_type'     => 'post',
		'supports'            => array(
			'title',
			'editor',
			'author',
			'thumbnail',
			'excerpt',
			'custom-fields',
			'trackbacks',
			'comments',
			'revisions',
			'page-attributes',
			'post-formats',
		),
	);
	register_post_type( 'banner', $args );
	register_taxonomy( 'banner_category', 'banner', array( 'hierarchical' => true, 'label' => __('Hệ thống Banner'), 'rewrite' => array( 'slug' => __('banner') ) ) );
}
add_action( 'init', 'banner' );
/**
 *
 * Limit word
 */
function cosmetic_limit_words($string, $word_limit)
{
  $words = explode(' ', $string, ($word_limit + 1));
  if(count($words) > $word_limit) {
  array_pop($words);
  echo implode(' ', $words)."..."; } else {
  echo implode(' ', $words); }
}
/**
**WOOCOMMERCE
**
*/
function mytheme_add_woocommerce_support() {
  add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );
add_action( 'after_setup_theme', 'yourtheme_setup' );
function yourtheme_setup() {
add_theme_support( 'wc-product-gallery-zoom' );
add_theme_support( 'wc-product-gallery-lightbox' );
add_theme_support( 'wc-product-gallery-slider' );
}
/*REMOVE tag, category, sku*/
// remove_action('woocommerce_single_product_summary','woocommerce_template_single_meta',40);
add_action('woocommerce_after_shop_loop_item','replace_add_to_cart');
function replace_add_to_cart() {
    global $product;
    $link = $product->get_permalink();
    echo do_shortcode('<a href="'.$link.'" class="button addtocartbutton">Learn more</a>');
}
// edit add to cart
add_filter( 'add_to_cart_text', 'woo_custom_cart_button_text' );                        // < 2.1
add_theme_support( 'woocommerce', array(
'thumbnail_image_width' => 150,
'single_image_width' => 322,
) ); 
//edit
add_filter( 'woocommerce_product_single_add_to_cart_text', 'woo_custom_cart_button_text' );
function woo_custom_cart_button_text() {
        return __( 'Thêm vào giỏ hàng', 'woocommerce' );
}
//nut mua ngay
function add_content_after_addtocart() {
  $current_product_id = get_the_ID();
  $product = wc_get_product( $current_product_id );
  $checkout_url = WC()->cart->get_checkout_url();
  if( $product->is_type( 'simple' ) ){
  echo '<span class="muangay"><a href="'.$checkout_url.'?add-to-cart='.$current_product_id.'" class="single_add_to_cart_button button alt">Mua ngay</a></span>';
  }
}
add_action( 'woocommerce_after_add_to_cart_button', 'add_content_after_addtocart' );

// -------------
// 1. Show Buttons
 
add_action( 'woocommerce_after_add_to_cart_quantity', 'bbloomer_display_quantity_plus' );
 
function bbloomer_display_quantity_plus() {
    echo '<button type="button" class="plus" >+</button>';
}
 
add_action( 'woocommerce_after_add_to_cart_quantity', 'bbloomer_display_quantity_minus' );
 
function bbloomer_display_quantity_minus() {
    echo '<button type="button" class="minus" >-</button><div class="clearfix"></div>';
}

// 2. Trigger jQuery script
 
add_action( 'wp_footer', 'bbloomer_add_cart_quantity_plus_minus' );
 
function bbloomer_add_cart_quantity_plus_minus() {
    // Only run this on the single product page
    if ( ! is_product() ) return;
    ?>
        <script type="text/javascript">
             
        jQuery(document).ready(function($){ 
             
            $('form.cart').on( 'click', 'button.plus, button.minus', function() {
 
                // Get current quantity values
                var qty = $( this ).closest( 'form.cart' ).find( '.qty' );
                var val = parseFloat(qty.val());
                var max = parseFloat(qty.attr( 'max' ));
                var min = parseFloat(qty.attr( 'min' ));
                var step = parseFloat(qty.attr( 'step' ));
 
                // Change the value if plus or minus
                if ( $( this ).is( '.plus' ) ) {
                    if ( max && ( max <= val ) ) {
                        qty.val( max );
                    } else {
                        qty.val( val + step );
                    }
                } else {
                    if ( min && ( min >= val ) ) {
                        qty.val( min );
                    } else if ( val > 1 ) {
                        qty.val( val - step );
                    }
                }
                 
            });
             
        });
             
        </script>
    <?php
}
/**
 * Change number of related products output
 */ 
function woo_related_products_limit() {
  global $product;
	
	$args['posts_per_page'] = 12;
	return $args;
}
add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args', 20 );
  function jk_related_products_args( $args ) {
	$args['posts_per_page'] = 6; // 6 related products
	$args['columns'] = 2; // arranged in 2 columns
	return $args;
}
//remove breadcrum
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);
//add item add to cart
add_filter('add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment');

function woocommerce_header_add_to_cart_fragment( $fragments ) {
  global $woocommerce;

  ob_start();

  ?>
  <span class="cart-count">[<?php echo $woocommerce->cart->cart_contents_count; ?> Items]</span>
  <?php

  $fragments['span.cart-count'] = ob_get_clean();

  return $fragments;

}
/**
 * Classic Editor.
 */
add_filter('use_block_editor_for_post', '__return_false');

// Thay doi duong dan logo admin
function wpc_url_login(){
return "http://localhost/cosmetic-store/"; // duong dan vao website cua ban
}
add_filter('login_headerurl', 'wpc_url_login');

// Thay doi logo admin wordpress
function login_css() {
wp_enqueue_style( 'login_css', get_template_directory_uri() . '/login.css' ); // duong dan den file css moi
}
add_action('login_head', 'login_css');


//filter search
//
add_action( 'pre_get_posts', 'advanced_search_query' );
function advanced_search_query( $query ) {

    if ( isset( $_REQUEST['search'] ) && $_REQUEST['search'] == 'advanced' && ! is_admin() && $query->is_search && $query->is_main_query() ) {

        $query->set( 'post_type', 'product' );

        $_model = $_GET['model'] != '' ? $_GET['model'] : '';

        $meta_query = array(
                array(
                    'key'     => 'car_model', // assumed your meta_key is 'car_model'
                    'value'   => $_model,
                    'compare' => 'LIKE', // finds models that matches 'model' from the select field
                )
            )
        ;
        $query->set( 'meta_query', $meta_query );

    }
}