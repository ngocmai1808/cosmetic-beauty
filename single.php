<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package cosmetic
 */
get_header();
?>
<div class="breadcrumbs">
	<?php
		if ( is_singular() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;
		?>
<?php
if ( function_exists('yoast_breadcrumb') ) {
yoast_breadcrumb('
<p id="breadcrumbs">','</p>');
}
?>
</div>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-9 col-md-push-3">
			<div id="primary" class="content-area container-fluid">
				<main id="main" class="site-main">

				<?php
				while ( have_posts() ) :
					the_post();

					get_template_part( 'template-parts/content' );

					// the_post_navigation();

				endwhile; // End of the loop.
				?>

				</main><!-- #main -->
			</div><!-- #primary -->

		</div>
		<div class="col-md-3 col-md-pull-9">
			<?php get_sidebar();?>
		</div>
	</div>
	<div class="related-post">
		<h3 class="title-h3">Bài viết liên quan</h3>
		 <?php $related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'numberposts' => 3, 'post__not_in' => array($post->ID) ) ); ?>
                <div class="row no-gutters">
                    <div class="content" role="main">
                    <?php if( $related ) foreach( $related as $post ) { setup_postdata($post); ?>
                    	<div class="col-md-4">
                    		 <?php
	                            get_template_part( 'template-parts/content', 'post' );
	                        ?>
                    	</div>
                       
                    <?php } wp_reset_postdata(); ?>
                    </div>
                </div>
	
	</div>
</div>
<?php
get_footer();
